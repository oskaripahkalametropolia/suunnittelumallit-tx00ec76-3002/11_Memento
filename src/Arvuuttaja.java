import java.util.Random;

public class Arvuuttaja {
    private final int bound;
    private final Random random;

    public Arvuuttaja(int bound){
        this.bound = bound;
        this.random = new Random();
    }

    public int getBound(){
        return bound;
    }

    public Object liityPeliin(){
        return new Memento(random.nextInt(bound));
    }

    public boolean arvaa(int arvaus, Object memento){
        Memento arvausMemento = (Memento) memento;
        return arvausMemento.getState() == arvaus;
    }

    private static class Memento{
        private final int state;
        public Memento(int state){
            this.state = state;
        }
        public int getState(){
            return this.state;
        }
    }
}
