import java.util.Random;

public class Asiakas implements Runnable{
    private final Arvuuttaja arvuuttaja;
    private final int bound;
    private final Object memento;
    private final Random random;

    public Asiakas(Arvuuttaja arvuuttaja){
        this.arvuuttaja = arvuuttaja;
        this.bound = this.arvuuttaja.getBound();
        this.memento = this.arvuuttaja.liityPeliin();
        this.random = new Random();
    }

    @Override
    public void run() {
        boolean oikein = false;
        while (!oikein){
            oikein = arvuuttaja.arvaa(random.nextInt(bound), memento);
        }
        System.out.printf("%s arvasi oikein!%n", this);
    }
}
