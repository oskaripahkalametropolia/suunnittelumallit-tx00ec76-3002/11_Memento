public class Main {
    public static void main(String[] args) {
        int n = 15;
        int bound = 10000000;

        Arvuuttaja arvuuttaja = new Arvuuttaja(bound);

        Asiakas[] asiakkaat = new Asiakas[n];

        for (int i = 0; i < asiakkaat.length; i++) {
            asiakkaat[i] = new Asiakas(arvuuttaja);
            asiakkaat[i].run();
        }
    }
}
